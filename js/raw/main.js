$ = jQuery;

$(document).on('ready', ready);
$(window).on('scroll', scrollFunc);
$(window).on('resize', resize);

$('.popup').click(function() {

    var target = $(this).attr('href');
    $.magnificPopup.open({
        items: {
            src: target,
            type: 'inline'
        }
    });

    return false;
});

$('.header-menu a').click(function() {

    var block = $(this).attr('href');
    if (block == '#')
        return false;

    if ($(window).width() > 1000) {
        var header = 74;
    } else {
        var header = 54;
    }

    var path = $(block).offset().top - header;
    $('body, html').animate({
        scrollTop: path
    }, 800);

    return false;
});

$('form').submit(function() {

    var form = $(this);
    var formTarget = $(this).attr('action');
    if (!formTarget)
        formTarget = 'send.php';
    var formData = form.serialize();
    var submitText = form.find('[type=submit]').val();

    if (form.hasClass('calc-body-form')) {
        var check = validateCalc();
        if (check == false) {
            return false;
        }
    }

    form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');

    if (formData) {
        $.post(formTarget, formData, function(data) {
            if (data == 'sended') {
                $.magnificPopup.open({
                    items: {
                        src: '#thanks',
                        type: 'inline'
                    }
                });
            } else {
                console.log(data);
            }
        });
    }

    form.find('input[type=submit]').removeAttr('disabled').val(submitText);

    return false;
});

$('.promo-menu-title').click(function() {

    $('.promo-menu').toggleClass('visible');
});

function ready() {

    if ($(window).scrollTop() > 1) {
        $('header').addClass('dark');
    } else {
        $('header').removeClass('dark');
    }

    var slides = 1;

    // if ($(window).width() <= 1000) {
    //     slides = 1;
    // } else if ($(window).width() <= 1200) {
    //     slides = 5;
    // } else {
    //     slides = 7;
    // }

    $('.portfolio-slider').slick({
        prevArrow: '<span class="slider-prev"></span>',
        nextArrow: '<span class="slider-next"></span>',
        dots: true,
        slidesToShow: slides
    });

    $('.reviews-slider').slick({
        prevArrow: '<span class="slider-prev"></span>',
        nextArrow: '<span class="slider-next"></span>',
        dots: false,
        slidesToShow: 1
    });

    $('input[name=phone]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});
}

function scrollFunc() {

    if ($(window).scrollTop() > 1) {
        $('header').addClass('dark');
    } else {
        $('header').removeClass('dark');
    }
}

function validateCalc() {

    if ($('[name="workType"]').val() == '' || $('[name="serviceType"]').val() == '' || $('[name="bottomType"]').val() == '' || $('[name="sizeWidth"]').val() == '' || $('[name="sizeDepth"]').val() == '' || $('[name="sizeHeight"]').val() == '') {
        alert('Пожалуйста, перед отправкой укажите тип укрепления/водоема/работ, тип грунта и размеры водоема.');
        return false;
    }
    return true;
}

$(document).on('click', '.calc-tab:not(.active)', function() {

    var type = $(this).attr('data-value');
    $('.calc-tab, .calc-params-col.changable').removeClass('active');
    $('.changable:not(.active) .calc-params-list li').removeClass('selected');
    $(this).addClass('active');
    $('[data-type="' + type + '"]').addClass('active');
    $('input[name="workType"]').val(type);
});

$(document).on('click', '.changable.active .calc-params-list li', function() {

    var type = $('.calc-tab.active').attr('data-value');
    var serviceType = $(this).text();
    var serviceName = $('.changable.active .calc-params-title').text();
    $('.changable.active .calc-params-list li').removeClass('selected');
    $(this).addClass('selected');
    $('input[name="workType"]').val(type);
    $('input[name="serviceType"]').val(serviceName + ': ' + serviceType);
});

$(document).on('click', '.calc-params-bottom li', function() {

    var type = $('.calc-tab.active').attr('data-value');
    var bottomType = $(this).text();
    $('.calc-params-bottom li').removeClass('selected');
    $(this).addClass('selected');
    $('input[name="workType"]').val(type);
    $('input[name="bottomType"]').val(bottomType);
});

$(document).on('click', '.calc-params-technic', function() {

    $(this).toggleClass('selected');
    if ($(this).hasClass('selected')) {
        $('input[name="calcForTechnic"]').val(1);
    } else {
        $('input[name="calcForTechnic"]').val(0);
    }
});

// #scrollspy
$('.animate').on('scrollSpy:enter', function() {

   if (!$(this).hasClass('visible')) {
       $(this).addClass('visible');
   }

});

$('.advantages-list li').eq(0).on('scrollSpy:enter', function() {

    var speed = 80;

    if (!$(this).hasClass('visible')) {
        $(this).addClass('visible');
        $(".advantages-list li").eq(0).stop().animate({'opacity': 1}, speed).addClass('visible');
        $(".advantages-list li").eq(1).stop().delay(speed*2).animate({'opacity': 1}, speed).addClass('visible');
        $(".advantages-list li").eq(2).stop().delay(speed*3).animate({'opacity': 1}, speed).addClass('visible');
        $(".advantages-list li").eq(3).stop().delay(speed*4).animate({'opacity': 1}, speed).addClass('visible');
        $(".advantages-list li").eq(4).stop().delay(speed*5).animate({'opacity': 1}, speed).addClass('visible');
        $(".advantages-list li").eq(5).stop().delay(speed*6).animate({'opacity': 1}, speed).addClass('visible');
    }
});

$('.steps-list .step').eq(0).on('scrollSpy:enter', function() {

    var speed = 50;

    if (!$(this).hasClass('visible')) {
        $(this).addClass('visible');
        $(".steps-list .step").eq(0).stop().animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow").eq(0).stop().delay(speed).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(1).stop().delay(speed*2).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow").eq(1).stop().delay(speed*3).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(2).stop().delay(speed*4).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow").eq(2).stop().delay(speed*5).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(3).stop().delay(speed*6).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow").eq(3).stop().delay(speed*7).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(4).stop().delay(speed*8).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow.bottom").eq(0).stop().delay(speed*9).animate({transform: 'rotate(90deg) scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(9).stop().delay(speed*10).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow.reverse").eq(3).stop().delay(speed*11).animate({transform: 'rotate(180deg) scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(8).stop().delay(speed*12).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow.reverse").eq(2).stop().delay(speed*13).animate({transform: 'rotate(180deg) scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(7).stop().delay(speed*14).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow.reverse").eq(1).stop().delay(speed*15).animate({transform: 'rotate(180deg) scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(6).stop().delay(speed*16).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step-arrow.reverse").eq(0).stop().delay(speed*17).animate({transform: 'rotate(180deg) scale(1)'}, speed*1.5).addClass('visible');
        $(".steps-list .step").eq(5).stop().delay(speed*18).animate({transform: 'scale(1)'}, speed*1.5).addClass('visible');
    }
});

$('.animate, .advantages-list li, .steps-list .step').scrollSpy();


function scroll() {}

function resize() {}

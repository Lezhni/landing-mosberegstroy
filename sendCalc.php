<?php

if ($_POST) {
    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('stroy-bereg@mail.ru', 'Мосберегстрой');
    $mail->addReplyTo('stroy-bereg@mail.ru', 'Мосберегстрой');
    $mail->addAddress('stroy-bereg@mail.ru');
    $mail->isHTML(true);

    $mail->Subject = 'Расчет работ с лендинга';

    $mail->Body = "<b>Имя:</b> {$_POST['contact']}<br><br><b>Номер телефона:</b> {$_POST['phone']}<br><br><b>Вид работ:</b> {$_POST['workType']}<br><br><b>Тип:</b> {$_POST['serviceType']}";
    $mail->Body .= "<br><br><b>Тип грунта:</b> {$_POST['bottomType']}<br><br><b>Высота берега, м:</b> {$_POST['sizeHeight']}<br><br><b>Глубина водоема, м:</b> {$_POST['sizeDepth']}<br><br><b>Длина береговой линии, м:</b> {$_POST['sizeWidth']}";
    if ((int) $_POST['calcForTechnic'] == 1) {
        $mail->Body .= "<br><br><b>Подсчеты для техники:</b> Да";
    }
    if($mail->send()) {
        echo 'sended';
    } else {
        echo $mail->ErrorInfo;
    }
}

die();
